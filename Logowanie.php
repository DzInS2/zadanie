<center>
<h1>Login</h1>
<form method="post">
  <input type="text" name="login" placeholder="Login"><br><br>
  <input type="password" name="password" placeholder="Password"><br><br>
  <button type="submit" name="zaloguj">Zaloguj</button>
  <h4>Nie masz konta?</h4><a href="http://localhost/Projekt/Rejstracja.php">Zarejestruj się!</a>
</form>
</center>
<?php
session_start();
require_once("Connection.php");
if(isset($_POST['zaloguj'])){
  $login = trim($_POST['login']);
  $password = trim($_POST['password']);

  $sth = $db->prepare('SELECT * FROM user WHERE login=:login limit 1');
  $sth->bindValue(':login', $login, PDO::PARAM_STR);
  $sth->execute();
  $user = $sth->fetch(PDO::FETCH_ASSOC);
  if($user){
    if(password_verify($password,$user['password'])){
		$_SESSION['zalogowany']=true;
      header('Location: http://localhost/Projekt/test.php');
    exit();
    }else{
      echo "<h3>Nieprawidlowe haslo</h3>";
    }
  }else{
    echo "<h3>Nie znaleziono uzytkownika</h3>";
  }
}
?>
