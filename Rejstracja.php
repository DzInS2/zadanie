<?php
session_start();
require_once("Connection.php");
if(isset($_POST['register'])){
    
  $login = $_POST['login'];
  $name = $_POST['name'];
  $surname = $_POST['surname'];
  $email = $_POST['email'];
  $password = $_POST['password'];
 
  $hashPassword = password_hash($password,PASSWORD_BCRYPT);

  $sth = $db->prepare('INSERT INTO user (login,name,surname,email,password) VALUE (:login,:name,:surname,:email,:password)');
  $sth->bindValue(':login', $login, PDO::PARAM_STR);
  $sth->bindValue(':name', $name, PDO::PARAM_STR);
  $sth->bindValue(':surname', $surname, PDO::PARAM_STR);
  $sth->bindValue(':email', $email, PDO::PARAM_STR);
  $sth->bindValue(':password', $hashPassword, PDO::PARAM_STR);
  $sth->execute();

 
  header('Location: http://localhost/Projekt/Logowanie.php');
    exit();
}
?>
<center>
<h1>Formularz rejestracyjny</h1>
<form method="post">
    <input type="text" name="login" placeholder="Login"><br><br>
    <input type="text" name="name" placeholder="Name"><br><br>
    <input type="text" name="surname" placeholder="Surname"><br><br>
    <input type="text" name="email" placeholder="Email"><br><br>
    <input type="password" name="password" placeholder="Password"><br><br>
  <button type="submit" name="register">Zarejestruj</button>
</form>
</center>